package rybak.education.domain;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}