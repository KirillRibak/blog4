package rybak.education.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rybak.education.domain.User;
import rybak.education.exception.ResourceNotFoundException;
import rybak.education.repository.UserRepository;

import javax.validation.Valid;

@RestController
@RequestMapping("user")
public class UserController {

    private UserRepository userRepo;

    @Autowired
    public void setRepo(UserRepository repo) {
        this.userRepo = repo;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public Page<User> list(Pageable pageable) {
        return userRepo.findAllAndReturnPage(pageable);
    }

    @GetMapping("{id}")
    public User getOne(@PathVariable("id") User user) {
        if (!userRepo.existsById(user.getId())) {
            throw new ResourceNotFoundException();
        }
        return userRepo.getOne(user.getId());
    }

    @PutMapping("{id}")
    public User update(@PathVariable("id") User userFromDb, @Valid @RequestBody User user) {
        BeanUtils.copyProperties(user, userFromDb, "id");
        return userRepo.save(userFromDb);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") User user) {
        userRepo.delete(user);
    }
}
