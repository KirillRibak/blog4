package rybak.education.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rybak.education.domain.Comment;
import rybak.education.exception.ResourceNotFoundException;
import rybak.education.repository.CommentRepository;
import rybak.education.repository.PostRepository;
import rybak.education.repository.UserRepository;

import javax.validation.Valid;

@RestController
public class CommentController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    @GetMapping("/user/{userId}/posts/{postId}/comments")
    public Page<Comment> getAllCommentsByPostId(@PathVariable("userId") Long userId,
                                                @PathVariable("postId") Long postId,
                                                Pageable pageable) {
        return commentRepository.findByUserIdAndPostId(userId, postId, pageable);
    }

    @PostMapping("/user/{userId}/posts/{postId}/comments")
    public Comment createComment(@PathVariable("userId") Long userId,
                                 @PathVariable("postId") Long postId,
                                 @Valid @RequestBody Comment comment) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id" + userId + "not found");
        }
        return postRepository.findById(postId).map(post -> {
            comment.setPost(post);
            return commentRepository.save(comment);
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + postId + " not found"));
    }

    @PutMapping("/user/{userId}/posts/{postId}/comments/{commentId}")
    public Comment updateComment(@PathVariable("userId") Long userId,
                                 @PathVariable("postId") Long postId,
                                 @PathVariable("commentId") Long commentId,
                                 @Valid @RequestBody Comment commentRequest) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id " + userId + " not found");
        }
        return commentRepository.findById(postId).map(comment -> {
            comment.setText(commentRequest.getText());
            return commentRepository.save(comment);
        }).orElseThrow(() -> new ResourceNotFoundException("Post id " + postId + "not found"));
    }

    @DeleteMapping("/user/{userId}/posts/{postId}/comments/{commentId}")
    public ResponseEntity<?> deleteComment(@PathVariable("userId") Long userId,
                                           @PathVariable("postId") Long postId,
                                           @PathVariable("commentId") Long commentId) {
        return commentRepository.findByIdAndPostIdAndUserId(commentId, postId, userId).map(comment -> {
            commentRepository.delete(comment);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Comment not found with id " + commentId + " and post id "
                + postId + "and user id" + userId));
    }
}
