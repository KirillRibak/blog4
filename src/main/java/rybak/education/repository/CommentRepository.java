package rybak.education.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rybak.education.domain.Comment;

import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("select  c from Comment c where c.post.id = :postId and c.post.author.id= :userId  ")
    Page<Comment> findByUserIdAndPostId( @Param("userId") Long userId, @Param("postId")Long postId,Pageable pageable);

    @Query("select c from Comment c where c.id= :id and c.post.id = :postId and c.post.author.id= :userId ")
    Optional<Comment> findByIdAndPostIdAndUserId(@Param("id") Long id,@Param("postId")Long postId, @Param("userId")Long userId);
}
