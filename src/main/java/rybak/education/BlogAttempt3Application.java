package rybak.education;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class BlogAttempt3Application {

    public static void main(String[] args) {
        SpringApplication.run(BlogAttempt3Application.class, args);
    }

}
